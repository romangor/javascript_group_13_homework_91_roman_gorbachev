const express = require('express');
const {nanoid} = require('nanoid');
const cors = require('cors');

const app = express();

require('express-ws')(app);

const port = 8000;
app.use(cors());

const activeConnections = {};
const savedCoordinates = [];

app.ws('/draw', (ws, req) => {
    const id = nanoid();
    activeConnections[id] = ws;

    ws.send(JSON.stringify({
        type: 'PREV_PIXELS',
        coordinates: savedCoordinates,
    }));

    ws.on('message', msg => {
        const decodedMessage = JSON.parse(msg);
        if (decodedMessage.type === 'SEND_PIXEL') {
            Object.keys(activeConnections).forEach(key => {
                const connected = activeConnections[key];
                savedCoordinates.push(decodedMessage.coordinates);
                connected.send(JSON.stringify({
                    type: 'NEW_PIXEL',
                    pixelCoordinates: decodedMessage.coordinates
                }));
            })
        }
    })

    ws.on('close', () => {
        delete activeConnections[id];
    })
})

app.listen(port, () => {
    console.log('Listen on port ' + port);
})