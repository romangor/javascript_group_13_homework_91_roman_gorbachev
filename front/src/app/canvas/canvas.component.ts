import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';
import { Pixel } from './pixel.model';


interface ServerMessage {
  type: string,
  coordinates: Pixel[],
  pixelCoordinates: Pixel;
}

@Component({
  selector: 'app-canvas',
  templateUrl: './canvas.component.html',
  styleUrls: ['./canvas.component.css']
})
export class CanvasComponent implements AfterViewInit {
  ws!: WebSocket;
  @ViewChild('canvas') canvas!: ElementRef;

  title = 'colorPicker';
  color: string = 'black';


  constructor() {
  }

  ngAfterViewInit() {
    const canvas: HTMLCanvasElement = this.canvas.nativeElement;
    this.ws = new WebSocket('ws://localhost:8000/draw');
    this.ws.onclose = () => console.log('ws_closed')

    this.ws.onmessage = event => {
      const decodedMessage: ServerMessage = JSON.parse(event.data);
      if (decodedMessage.type === 'PREV_PIXELS') {
        decodedMessage.coordinates.forEach(point => {
          this.drawPoint(point.x, point.y, point.color)
        })
      }

      if (decodedMessage.type === 'NEW_PIXEL') {
        const {x, y, color} = decodedMessage.pixelCoordinates;
        this.drawPoint(x, y, this.color);
      }
    }

  };

  drawPoint(x: number, y: number, color: string) {
    const canvas: HTMLCanvasElement = this.canvas.nativeElement;
    const ctx = canvas.getContext('2d')!;
    ctx.beginPath();
    ctx.arc(x, y, 10, 0, 2 * Math.PI);
    ctx.strokeStyle = color;
    ctx.stroke();
  };

  canvasClick(event: MouseEvent) {
    const x = event.offsetX
    const y = event.offsetY
    this.ws.send(JSON.stringify({
      type: 'SEND_PIXEL',
      coordinates: new Pixel(x, y, this.color)
    }));
  };

}
